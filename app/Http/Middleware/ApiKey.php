<?php

namespace App\Http\Middleware;

use App\ApiKeys;
use Closure;

class ApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check to see if the apikey is sent via headers
        if($request->headers->has('apikey')){
            // get the api key
            $key = $request->headers->get('apikey');
            $keycheck = ApiKeys::where('key', '=', $key)->first();
            // check to see if the api key is one that we have in out DB
            if($keycheck !== null){
                //Move to the next step
                return $next($request);
            }

//            notify the user that the api key is invalid
            return response('The Api Key provided is not valid!', 404);
        }else{
            // notify the user that they need to provide an api key
            return response('Please make sure that you have set eh `apikey` header!',403);
        }

    }
}
