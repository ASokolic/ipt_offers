<?php

namespace App\Http\Controllers\Api;

use App\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductUploadController extends Controller
{
    //

    /**
     * This method will handle the product upload (File)
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function upload(Request $request){

        // get the file that has been uploaded
        $file = $request->file('file');

        // check to see if there is a file
        if (!$request->file('file')){
            // notify the user that the file needs to be supplied
            return response('No file provided!',403);
        }

        // use the xml decoder so that we have a nice object to work with
        $xml = simplexml_load_file($file);

        // loop the products and save them into the data base
        foreach ($xml->new_products->product as $xmlProduct){

            // NOTE: we can add further product verification and checks but for the sake of the brief given this is a simple solution

            $product = new Products();
            $product->name = $xmlProduct->name;
            $product->code = $xmlProduct->code;
            $product->description = $xmlProduct->description;
            $product->price = $xmlProduct->price;
            $product->save();
        }

        // nitify the user that the upload worked
        return response('Products have been uploaded',200);

    }
}
