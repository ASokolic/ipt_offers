<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiKeys extends Model
{
    //
    //
    protected $table = 'api_key';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'vendor_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
