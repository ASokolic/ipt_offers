<h2>To View front end</h2>
run the below one after another and the app should run in no time at all.
<br>
<p><b>to clone:</b> git clone https://ASokolic@bitbucket.org/ASokolic/ipt_offers.git</p>
<p><b>composer install</b></p>
<p><b>php artisan migrate</b></p>
<p><b>php artisan db:seed</b></p>
<p><b>npm install</b></p>
<p><b>npm run dev/prod</b></p>
<p><b>php artisan serve</b></p>
<p><b>NOTE:</b> i was having some CORS issue with the front end and i wanst able to test the axios/ajax call that uses the main API endpoint properly.</p>
<h2>use API</h2>
<p>[POST].../api/products.<br>
Extra header param for api_key
<ul>
<li>apikey</li>
</ul>
parameters
<ul>
<li>file (this is what name you need to assign to the file upload for the xml file)</li>
</ul>
</p>
example:
POST /api/products HTTP/1.1
Host: 127.0.0.1:8000
apikey: H50y33eUcs3NeqZ4hNwraHKllfisW55d
Cache-Control: no-cache
Postman-Token: d3b9fdad-3152-41a7-b4e1-d3eb37fbbf31
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="file"; filename="xmlexample.xml"
Content-Type: text/xml


------WebKitFormBoundary7MA4YWxkTrZu0gW--

<h2>Where to long for my logic</h2>
<ul>
    <li>app/ApiKeys.php</li>
    <li>app/Products.php</li>
    <li>app/Controllers/Api/ProductUploadController.php</li>
    <li>app/Middleware/ApiKey.php</li>
    <li>database/migrations/*.php</li>
    <li>database/seeds/api_keys.php</li>
    <li>resources/views/welcome.blade.php</li>
    <li>resources/js/app.js</li>
    <li>resources/js/components/ExampleComponent.vue</li>
    <li>routes/api.php</li>
    <li>routes/web.php</li>
</ul>